# Destiny: Universe of Light

Jouez en ligne avec vos amis, récupérez tout les éléments dont vous avez besoin pour tenir une partie en ligne de *Destiny: Universe of Light* !<br/>
Play online with your friends, collect all the items you need to start an online game, and play *Destiny: Universe of Light* as a game master !


## Bienvenue ! / Welcome!
Le jeu de rôle *Destiny: Universe of Light* est un jeu de rôle basé sur l’univers de Destiny, les jeux FPS à succès créées par Bungie™. Vous trouverez ici tous les documents ainsi que toutes les informations pour récupérer et utiliser l'intégration FoundryVTT, vous permettant de facilement découvrir et jouer au jeu de rôle *Destiny: Universe of Light* en ligne avec vos amis.<br/>
The tabletop role-playing game *Destiny: Universe of Light* is based on the Destiny universe, the successful FPS series created by Bungie™. Here, you will find all the documents as well as all the information to retrieve and use the FoundryVTT integration, allowing you to easily discover and play *Destiny: Universe of Light* online with your friends.<br/>


## Livre du Joueur / Player's Book

[Livre du Joueur / Player's Book](https://docs.google.com/document/d/1e1_MZhRqyfOBCL9eYWfoC2HOatwqKUFwQA8sDc7aOT4/edit?usp=sharing)<br/>


## Livre du Maître du Jeu / Game Master's Book

[Livre du Maître du Jeu / Game Master's Book](https://docs.google.com/document/d/1pHaO093wYfL7qcrGAZW6aBeeLH_vymeoHgIxiKNd-qc/edit?usp=sharing)<br/>


## Arsenal / Arsenal

[Arsenal / Arsenal](https://docs.google.com/document/d/1jNcCwPXf_yzoFyfQUx-9NihEZp1TlzZHkHZtW29RLbY/edit?usp=sharing)<br/>


## Bestiaire / Bestiary

[Bestiaire / Bestiary](https://docs.google.com/document/d/1wKD0bnPU6uCn8coWkTxBJUK22Wdtflve_FjMyvqY9No/edit?usp=sharing)<br/>


## Fiches de Personnage / Character Sheets

[Fiches de Personnage / Character Sheets](https://docs.google.com/document/d/1jMtAYNsMh_w0y7fviUMZLSqaAK7yggyQGF6PKQCCS_w/edit?usp=sharing)<br/>


## Ressources / Resources

* [Site Web / Website](https://www.destinyjdr.com/)
* [Serveur Discord / Discord Server](https://discord.gg/DhTKfnd7Yd)
* Contact : benjamin.prieur@protonmail.com


## License

Ce travail, et *Destiny: Universe of Light* sont sous licence Creative Commons Attribution - CC0 1.0 Universel (CC0 1.0) France (https://creativecommons.org/publicdomain/zero/1.0/deed.fr)<br/>
This work and *Destiny: Universe of Light* are under the Creative Commons Attribution - CC0 1.0 Universel (CC0 1.0) France (https://creativecommons.org/publicdomain/zero/1.0/deed.fr)