export default class destinyjdrItem extends Item {
    chatTemplate = {
        "weapon": "systems/destinyjdr/templates/partials/weapon-card.html",
        "weaponChat": "systems/destinyjdr/templates/partials/weapon-chat.html",
        "armor": "systems/destinyjdr/templates/partials/armor-card.html",
        "perk": "systems/destinyjdr/templates/partials/perk-card.html",
        "spellcard": "systems/destinyjdr/templates/partials/spellcard-card.html"
    };

    async roll() {
        let chatData = {
            user: game.user._id,
            speaker: ChatMessage.getSpeaker()
        };

        let cardData = {
            ...this,
            owner: this.actor.id
        };

        chatData.content = await renderTemplate(this.chatTemplate[this.type], cardData);

        chatData.roll = true;

        return ChatMessage.create(chatData);
    }
}