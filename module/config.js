export const destinyjdr = {};

destinyjdr.racesLoc = {
  human: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  awaken: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  exo: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  fallen: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [9]},
      {id: 'object', range: [8]},
      {id: 'weapon', range: [7]},
      {id: 'rArm1', range: [6]},
      {id: 'lArm1', range: [5]},
      {id: 'rArm2', range: [4]},
      {id: 'lArm2', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  cabal: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  hive: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  vex:{
    locaList: [
      {id: 'head', range: [10]},
      {id: 'torso', crit: true, range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  scorn: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [9]},
      {id: 'object', range: [8]},
      {id: 'weapon', range: [7]},
      {id: 'rArm1', range: [6]},
      {id: 'lArm1', range: [5]},
      {id: 'rArm2', range: [4]},
      {id: 'lArm2', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  cloudstrider: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  light: {
    locaList: [
      {id: 'head', range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  darkness: {
    locaList: [
      {id: 'head', range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
  other: {
    locaList: [
      {id: 'head', crit: true, range: [10]},
      {id: 'torso', range: [7, 8, 9]},
      {id: 'object', range: [6]},
      {id: 'weapon', range: [5]},
      {id: 'rArm', range: [4]},
      {id: 'lArm', range: [3]},
      {id: 'rLeg', range: [2]},
      {id: 'lLeg', range: [1]}
    ]
  },
}

destinyjdr.stats = {
  combatskill: "destinyjdr.stats.combatskill",
  aimingskill: "destinyjdr.stats.aimingskill",
  strength: "destinyjdr.stats.strength",
  dexterity: "destinyjdr.stats.dexterity",
  resilience: "destinyjdr.stats.resilience",
  intellect: "destinyjdr.stats.intellect",
  perception: "destinyjdr.stats.perception",
  mentalstrength: "destinyjdr.stats.mentalstrength",
  charisma: "destinyjdr.stats.charisma",
  technology: "destinyjdr.stats.technology"
}

destinyjdr.sheet = {
  rollCombatskill: "destinyjdr.sheet.rollCombatskill",
  rollAimingskill: "destinyjdr.sheet.rollAimingskill",
  rollStrength: "destinyjdr.sheet.rollStrength",
  rollDexterity: "destinyjdr.sheet.rollDexterity",
  rollResilience: "destinyjdr.sheet.rollResilience",
  rollIntellect: "destinyjdr.sheet.rollIntellect",
  rollPerception: "destinyjdr.sheet.rollPerception",
  rollMentalstrength: "destinyjdr.sheet.rollMentalstrength",
  rollCharisma: "destinyjdr.sheet.rollCharisma",
  rollTechnology: "destinyjdr.sheet.rollTechnology"
}

destinyjdr.statsCondensed = {
  rollCS: "destinyjdr.statsCondensed.rollCS",
  rollAS: "destinyjdr.statsCondensed.rollAS",
  rollSTR: "destinyjdr.statsCondensed.rollSTR",
  rollDEX: "destinyjdr.statsCondensed.rollDEX",
  rollRES: "destinyjdr.statsCondensed.rollRES",
  rollINT: "destinyjdr.statsCondensed.rollINT",
  rollPER: "destinyjdr.statsCondensed.rollPER",
  rollMS: "destinyjdr.statsCondensed.rollMS",
  rollCH: "destinyjdr.statsCondensed.rollCH",
  rollTEC: "destinyjdr.statsCondensed.rollTEC"
}

destinyjdr.classes = {
  hunter: "destinyjdr.class.hunter",
  titan: "destinyjdr.class.titan",
  warlock: "destinyjdr.class.warlock"
}

destinyjdr.genders = {
  male: "destinyjdr.genders.male",
  female: "destinyjdr.genders.female",
  other: "destinyjdr.genders.other"
}

destinyjdr.races = {
  human: "destinyjdr.races.human",
  awaken: "destinyjdr.races.awaken",
  exo: "destinyjdr.races.exo",
  fallen: "destinyjdr.races.fallen",
  cabal: "destinyjdr.races.cabal",
  hive: "destinyjdr.races.hive",
  vex: "destinyjdr.races.vex",
  scorn: "destinyjdr.races.scorn",
  cloudstrider: "destinyjdr.races.cloudstrider",
  light: "destinyjdr.races.light",
  darkness: "destinyjdr.races.darkness",
  other: "destinyjdr.races.other"
}

destinyjdr.hunterSubclasses = {
  gunslinger: "destinyjdr.subclass.gunslinger",
  knifejuggler: "destinyjdr.subclass.knifejuggler",
  arcstrider: "destinyjdr.subclass.arcstrider",
  nightstalker: "destinyjdr.subclass.nightstalker",
  infiltrator: "destinyjdr.subclass.infiltrator",
  revenant: "destinyjdr.subclass.revenant",
  threadrunner: "destinyjdr.subclass.threadrunner",
  hallowed: "destinyjdr.subclass.hallowed",
  successor: "destinyjdr.subclass.successor"
}

destinyjdr.titanSubclasses = {
  sunbreaker: "destinyjdr.subclass.sunbreaker",
  devastator: "destinyjdr.subclass.devastator",
  striker: "destinyjdr.subclass.striker",
  codeofthemissile: "destinyjdr.subclass.codeofthemissile",
  sentinel: "destinyjdr.subclass.sentinel",
  behemoth: "destinyjdr.subclass.behemoth",
  berserker: "destinyjdr.subclass.berserker",
  pioneer: "destinyjdr.subclass.pioneer",
  lancer: "destinyjdr.subclass.lancer"
}

destinyjdr.warlockSubclasses = {
  dawnblade: "destinyjdr.subclass.dawnblade",
  stormcaller: "destinyjdr.subclass.stormcaller",
  chaosseeker: "destinyjdr.subclass.chaosseeker",
  childofthevoid: "destinyjdr.subclass.childofthevoid",
  voidwalker: "destinyjdr.subclass.voidwalker",
  shadebinder: "destinyjdr.subclass.shadebinder",
  broodweaver: "destinyjdr.subclass.broodweaver",
  adoubant: "destinyjdr.subclass.adoubant",
  alchemage: "destinyjdr.subclass.alchemage"
}

destinyjdr.pathsHunter = {
  sharpshooters: "destinyjdr.pathHunter.sharpshooters",
  experienced: "destinyjdr.pathHunter.experienced",
  outlaws: "destinyjdr.pathHunter.outlaws",
  gamblers: "destinyjdr.pathHunter.gamblers",
  thousandcuts: "destinyjdr.pathHunter.thousandcuts",
  pyromaniac: "destinyjdr.pathHunter.pyromaniac",
  cunning: "destinyjdr.pathHunter.cunning",
  meditation: "destinyjdr.pathHunter.meditation",
  scout: "destinyjdr.pathHunter.scout",
  storm: "destinyjdr.pathHunter.storm",
  fighter: "destinyjdr.pathHunter.fighter",
  execution: "destinyjdr.pathHunter.execution",
  cartographer: "destinyjdr.pathHunter.cartographer",
  shadows: "destinyjdr.pathHunter.shadows",
  ambush: "destinyjdr.pathHunter.ambush",
  ghost: "destinyjdr.pathHunter.ghost",
  forsaken: "destinyjdr.pathHunter.forsaken",
  slaughterer: "destinyjdr.pathHunter.slaughterer",
  silence: "destinyjdr.pathHunter.silence",
  stealth: "destinyjdr.pathHunter.stealth",
  deadlyfrost: "destinyjdr.pathHunter.deadlyfrost",
  disaster: "destinyjdr.pathHunter.disaster",
  rope: "destinyjdr.pathHunter.rope",
  silk: "destinyjdr.pathHunter.silk",
  lasso: "destinyjdr.pathHunter.lasso",
  greatnet: "destinyjdr.pathHunter.greatnet",
  deconstruction: "destinyjdr.pathHunter.deconstruction",
  structure: "destinyjdr.pathHunter.structure",
  figures: "destinyjdr.pathHunter.figures",
  genesis: "destinyjdr.pathHunter.genesis",
  tormentors: "destinyjdr.pathHunter.tormentors",
  curses: "destinyjdr.pathHunter.curses",
  plans: "destinyjdr.pathHunter.plans",
  demons: "destinyjdr.pathHunter.demons"
}

destinyjdr.pathsTitan = {
  forge: "destinyjdr.pathTitan.forge",
  fortress: "destinyjdr.pathTitan.fortress",
  temperedsteel: "destinyjdr.pathTitan.temperedsteel",
  war: "destinyjdr.pathTitan.war",
  devastation: "destinyjdr.pathTitan.devastation",
  heavylancer: "destinyjdr.pathTitan.heavylancer",
  roar: "destinyjdr.pathTitan.roar",
  detonation: "destinyjdr.pathTitan.detonation",
  teaching: "destinyjdr.pathTitan.teaching",
  offensive: "destinyjdr.pathTitan.offensive",
  missile: "destinyjdr.pathTitan.missile",
  initiative: "destinyjdr.pathTitan.initiative",
  specialists: "destinyjdr.pathTitan.specialists",
  demolition: "destinyjdr.pathTitan.demolition",
  discipline: "destinyjdr.pathTitan.discipline",
  commander: "destinyjdr.pathTitan.commander",
  deathrattle: "destinyjdr.pathTitan.deathrattle",
  obelisk: "destinyjdr.pathTitan.obelisk",
  deadlyfrost: "destinyjdr.pathTitan.deadlyfrost",
  hostility: "destinyjdr.pathTitan.hostility",
  rope: "destinyjdr.pathTitan.rope",
  whip: "destinyjdr.pathTitan.whip",
  grip: "destinyjdr.pathTitan.grip",
  consolidation: "destinyjdr.pathTitan.consolidation",
  sharpness: "destinyjdr.pathTitan.sharpness",
  lucidity: "destinyjdr.pathTitan.lucidity",
  foundations: "destinyjdr.pathTitan.foundations",
  genesis: "destinyjdr.pathTitan.genesis",
  hierophant: "destinyjdr.pathTitan.hierophant",
  massacre: "destinyjdr.pathTitan.massacre",
  plans: "destinyjdr.pathTitan.plans",
  barbaricarts: "destinyjdr.pathTitan.barbaricarts"
}

destinyjdr.pathsWarlock = {
  firmament: "destinyjdr.pathWarlock.firmament",
  spirit: "destinyjdr.pathWarlock.spirit",
  dawn: "destinyjdr.pathWarlock.dawn",
  grace: "destinyjdr.pathWarlock.grace",
  sciences: "destinyjdr.pathWarlock.sciences",
  wrath: "destinyjdr.pathWarlock.wrath",
  induction: "destinyjdr.pathWarlock.induction",
  ions: "destinyjdr.pathWarlock.ions",
  intuition: "destinyjdr.pathWarlock.intuition",
  pulse: "destinyjdr.pathWarlock.pulse",
  fear: "destinyjdr.pathWarlock.fear",
  hunger: "destinyjdr.pathWarlock.hunger",
  secrets: "destinyjdr.pathWarlock.secrets",
  chaos: "destinyjdr.pathWarlock.chaos",
  infinitevoid: "destinyjdr.pathWarlock.infinitevoid",
  matter: "destinyjdr.pathWarlock.matter",
  instability: "destinyjdr.pathWarlock.instability",
  blizzard: "destinyjdr.pathWarlock.blizzard",
  withering: "destinyjdr.pathWarlock.withering",
  deadlyfrost: "destinyjdr.pathWarlock.deadlyfrost",
  logistic: "destinyjdr.pathWarlock.logistic",
  rope: "destinyjdr.pathWarlock.rope",
  prison: "destinyjdr.pathWarlock.prison",
  needles: "destinyjdr.pathWarlock.needles",
  patched: "destinyjdr.pathWarlock.patched",
  inventor: "destinyjdr.pathWarlock.inventor",
  adjuration: "destinyjdr.pathWarlock.adjuration",
  disaster: "destinyjdr.pathWarlock.disaster",
  genesis: "destinyjdr.pathWarlock.genesis",
  ritualist: "destinyjdr.pathWarlock.ritualist",
  hands: "destinyjdr.pathWarlock.hands",
  plans: "destinyjdr.pathWarlock.plans",
  horror: "destinyjdr.pathWarlock.horror"
}

destinyjdr.weaponTypes = {
  melee: "destinyjdr.weapontype.melee",
  explosive: "destinyjdr.weapontype.explosive",
  autorifle: "destinyjdr.weapontype.autorifle",
  submachinegun: "destinyjdr.weapontype.submachinegun",
  scoutrifle: "destinyjdr.weapontype.scoutrifle",
  shotgun: "destinyjdr.weapontype.shotgun",
  pulserifle: "destinyjdr.weapontype.pulserifle",
  sniperrifle: "destinyjdr.weapontype.sniperrifle",
  sidearm: "destinyjdr.weapontype.sidearm",
  handcannon: "destinyjdr.weapontype.handcannon",
  fusionrifle: "destinyjdr.weapontype.fusionrifle",
  linearfusionrifle: "destinyjdr.weapontype.linearfusionrifle",
  sword: "destinyjdr.weapontype.sword",
  grenadelauncher: "destinyjdr.weapontype.grenadelauncher",
  machinegun: "destinyjdr.weapontype.machinegun",
  glaive: "destinyjdr.weapontype.glaive",
  rocketlauncher: "destinyjdr.weapontype.rocketlauncher",
  bow: "destinyjdr.weapontype.bow"
}

destinyjdr.elements = {
  solar: "destinyjdr.weaponelement.solar",
  arc: "destinyjdr.weaponelement.arc",
  void: "destinyjdr.weaponelement.void",
  stasis: "destinyjdr.weaponelement.stasis",
  strand: "destinyjdr.weaponelement.strand",
  vertant: "destinyjdr.weaponelement.vertant",
  offaxis: "destinyjdr.weaponelement.offaxis"
}

destinyjdr.weaponElements = {
  kinetic: "destinyjdr.weaponelement.kinetic",
  solar: "destinyjdr.weaponelement.solar",
  arc: "destinyjdr.weaponelement.arc",
  void: "destinyjdr.weaponelement.void",
  stasis: "destinyjdr.weaponelement.stasis",
  strand: "destinyjdr.weaponelement.strand",
  vertant: "destinyjdr.weaponelement.vertant",
  offaxis: "destinyjdr.weaponelement.offaxis"
}

destinyjdr.difficulties = {
  trivial: "destinyjdr.difficulties.trivial",
  elementary: "destinyjdr.difficulties.elementary",
  childish: "destinyjdr.difficulties.childish",
  veryeasy: "destinyjdr.difficulties.veryeasy",
  easy: "destinyjdr.difficulties.easy",
  simple: "destinyjdr.difficulties.simple",
  available: "destinyjdr.difficulties.available",
  affordable: "destinyjdr.difficulties.affordable",
  normal: "destinyjdr.difficulties.normal",
  pained: "destinyjdr.difficulties.pained",
  delicate: "destinyjdr.difficulties.delicate",
  laborious: "destinyjdr.difficulties.laborious",
  difficult: "destinyjdr.difficulties.difficult",
  verydifficult: "destinyjdr.difficulties.verydifficult",
  challenging: "destinyjdr.difficulties.challenging",
  infernal: "destinyjdr.difficulties.infernal",
  impossible: "destinyjdr.difficulties.impossible"
}

destinyjdr.damagelevels = {
  nothing: "destinyjdr.sheet.nothing",
  combatskill: "destinyjdr.damagelevel.combatskill",
  aimingskill: "destinyjdr.damagelevel.aimingskill",
  strength: "destinyjdr.damagelevel.strength",
  dexterity: "destinyjdr.damagelevel.dexterity",
  resilience: "destinyjdr.damagelevel.resilience",
  intellect: "destinyjdr.damagelevel.intellect",
  perception: "destinyjdr.damagelevel.perception",
  mentalstrength: "destinyjdr.damagelevel.mentalstrength",
  charisma: "destinyjdr.damagelevel.charisma",
  technology: "destinyjdr.damagelevel.technology",
  superpoints: "destinyjdr.damagelevel.superpoints"
}

destinyjdr.weaponScopes = {
  noscope: "destinyjdr.weaponscope.noscope",
  exotic: "destinyjdr.weaponscope.exotic",
  quickdotsas: "destinyjdr.weaponscope.quickdotsas",
  truesighthcs: "destinyjdr.weaponscope.truesighthcs",
  is5circle: "destinyjdr.weaponscope.is5circle",
  model8red: "destinyjdr.weaponscope.model8red",
  gbiron: "destinyjdr.weaponscope.gbiron",
  wolfsightw1: "destinyjdr.weaponscope.wolfsightw1",
  sdthermal: "destinyjdr.weaponscope.sdthermal",
  duskdotd1: "destinyjdr.weaponscope.duskdotd1",
  scholo: "destinyjdr.weaponscope.scholo",
  devilscoped2: "destinyjdr.weaponscope.devilscoped2",
  ldwatchdog: "destinyjdr.weaponscope.ldwatchdog",
  mark15lens: "destinyjdr.weaponscope.mark15lens",
  lcranged: "destinyjdr.weaponscope.lcranged",
  atblongrange: "destinyjdr.weaponscope.atblongrange"
}

destinyjdr.weaponScopesRanges = {
  noscope: "destinyjdr.weaponscoperange.noscoperange",
  exotic: "destinyjdr.weaponscoperange.exoticrange",
  quickdotsas: "destinyjdr.weaponscoperange.quickdotsasrange",
  truesighthcs: "destinyjdr.weaponscoperange.truesighthcsrange",
  is5circle: "destinyjdr.weaponscoperange.is5circlerange",
  model8red: "destinyjdr.weaponscoperange.model8redrange",
  gbiron: "destinyjdr.weaponscoperange.gbironrange",
  wolfsightw1: "destinyjdr.weaponscoperange.wolfsightw1range",
  sdthermal: "destinyjdr.weaponscoperange.sdthermalrange",
  duskdotd1: "destinyjdr.weaponscoperange.duskdotd1range",
  scholo: "destinyjdr.weaponscoperange.scholorange",
  devilscoped2: "destinyjdr.weaponscoperange.devilscoped2range",
  ldwatchdog: "destinyjdr.weaponscoperange.ldwatchdogrange",
  mark15lens: "destinyjdr.weaponscoperange.mark15lensrange",
  lcranged: "destinyjdr.weaponscoperange.lcrangedrange",
  atblongrange: "destinyjdr.weaponscoperange.atblongrangerange"
}

destinyjdr.weaponScopesMessages = {
  good: "destinyjdr.weaponscopemessage.good",
  bad1: "destinyjdr.weaponscopemessage.bad1",
  bad2: "destinyjdr.weaponscopemessage.bad2",
  bad3: "destinyjdr.weaponscopemessage.bad3",
  bad4: "destinyjdr.weaponscopemessage.bad4"
}

destinyjdr.rarityTypes = {
  worn: "destinyjdr.generalRarity.worn",
  goodquality: "destinyjdr.generalRarity.goodquality",
  outstandingquality: "destinyjdr.generalRarity.outstandingquality",
  masterpiece: "destinyjdr.generalRarity.masterpiece",
  exotic: "destinyjdr.generalRarity.exotic",
  enlightened: "destinyjdr.generalRarity.enlightened",
  sunk: "destinyjdr.generalRarity.sunk"
}

destinyjdr.gameplayParts = {
  actions: "destinyjdr.gameplayParts.actions"
}

destinyjdr.linkedTraits = {
  no: "destinyjdr.linkedTrait.noText",
  yes: "destinyjdr.linkedTrait.yesText",
  class: "destinyjdr.linkedTrait.classText"
}

destinyjdr.personalities = {
  grumpy: "destinyjdr.personality.grumpy",
  emotional: "destinyjdr.personality.emotional",
  puny: "destinyjdr.personality.puny",
  curious: "destinyjdr.personality.curious",
  hyperactive: "destinyjdr.personality.hyperactive",
  solitary: "destinyjdr.personality.solitary",
  responsible: "destinyjdr.personality.responsible",
  naive: "destinyjdr.personality.naive",
  faithful: "destinyjdr.personality.faithful",
  vicious: "destinyjdr.personality.vicious",
  ambitious: "destinyjdr.personality.ambitious",
  creative: "destinyjdr.personality.creative",
  brave: "destinyjdr.personality.brave",
  humble: "destinyjdr.personality.humble",
  integrates: "destinyjdr.personality.integrates",
  angry: "destinyjdr.personality.angry",
  competitive: "destinyjdr.personality.competitive",
  calm: "destinyjdr.personality.calm",
  inexperienced: "destinyjdr.personality.inexperienced",
  perfectionist: "destinyjdr.personality.perfectionist"
}

destinyjdr.weaponCrafters = {
  unknown: "destinyjdr.weaponcrafter.unknown",
  blackarmory: "destinyjdr.weaponcrafter.blackarmory",
  cassoid: "destinyjdr.weaponcrafter.cassoid",
  cruxlomar: "destinyjdr.weaponcrafter.cruxlomar",
  daito: "destinyjdr.weaponcrafter.daito",
  hakke: "destinyjdr.weaponcrafter.hakke",
  nadir: "destinyjdr.weaponcrafter.nadir",
  omolon: "destinyjdr.weaponcrafter.omolon",
  suros: "destinyjdr.weaponcrafter.suros",
  texmechanica: "destinyjdr.weaponcrafter.texmechanica",
  veist: "destinyjdr.weaponcrafter.veist",
  thetower: "destinyjdr.weaponcrafter.thetower",
  thedrifter: "destinyjdr.weaponcrafter.thedrifter",
  ironbanner: "destinyjdr.weaponcrafter.ironbanner",
  ishtarcollective: "destinyjdr.weaponcrafter.ishtarcollective",
  siva: "destinyjdr.weaponcrafter.siva",
  vex: "destinyjdr.weaponcrafter.vex",
  thehive: "destinyjdr.weaponcrafter.thehive",
  thelighthouse: "destinyjdr.weaponcrafter.thelighthouse",
  newempire: "destinyjdr.weaponcrafter.newempire",
  olusworkshop: "destinyjdr.weaponcrafter.olusworkshop",
  houseoflight: "destinyjdr.weaponcrafter.houseoflight",
  theleviathan: "destinyjdr.weaponcrafter.theleviathan",
  dreamingcity: "destinyjdr.weaponcrafter.dreamingcity",
  themoon: "destinyjdr.weaponcrafter.themoon",
  brayexpedition: "destinyjdr.weaponcrafter.brayexpedition",
  brayexoscience: "destinyjdr.weaponcrafter.brayexoscience",
  poelruiter: "destinyjdr.weaponcrafter.poelruiter",
  enigma: "destinyjdr.weaponcrafter.enigma",
  thesorrow: "destinyjdr.weaponcrafter.thesorrow",
  group935: "destinyjdr.weaponcrafter.group935"
}

destinyjdr.textData = {
  weaponTypeText: "destinyjdr.itemText.weaponTypeText",
  armorTypeText: "destinyjdr.itemText.armorTypeText",
  weaponElementText: "destinyjdr.itemText.weaponElementText",
  element: "destinyjdr.itemText.elementText",
  weaponCrafterText: "destinyjdr.itemText.weaponCrafterText",
  weaponCrafterClicText: "destinyjdr.itemText.weaponCrafterClicText",
  weaponRarityText: "destinyjdr.itemText.rarityText",
  weaponPenetration: "destinyjdr.itemText.weaponPenetration",
  weaponPenetrationText: "destinyjdr.itemText.weaponPenetrationText",
  weaponScopeText: "destinyjdr.itemText.weaponScopeText",
  duration: "destinyjdr.itemText.durationText",
  requiredLevel: "destinyjdr.itemText.requiredLevelText",
  damage: "destinyjdr.itemText.damageText",
  value: "destinyjdr.itemText.valueText",
  effect: "destinyjdr.itemText.effectText",
  effects: "destinyjdr.sheet.effectsText",
  idname: "destinyjdr.itemText.idname",
  idname: "destinyjdr.itemText.idname",
  tooltipWarning: "destinyjdr.itemText.tooltipWarning",
  perkname: "destinyjdr.itemText.perkname",
  cost: "destinyjdr.itemText.costText",
  rollcheck: "destinyjdr.itemText.rollcheck",
  difficulty: "destinyjdr.itemText.difficulty",
  linked: "destinyjdr.itemText.linkedText",
  weaponMagazine: "destinyjdr.itemText.weaponmagazine",
  weaponMagazineTotal: "destinyjdr.itemText.weaponmagazinetotal",
  weaponMagazineLeft: "destinyjdr.itemText.weaponmagazineleft",
  weaponRange: "destinyjdr.itemText.weaponrange",
  weaponRangeThrowing: "destinyjdr.itemText.rangeThrowText",
  description: "destinyjdr.itemText.description",
  itemperk: "destinyjdr.itemText.perkText",
  chosenStat: "destinyjdr.itemText.chosenStat",
  effect: "destinyjdr.itemText.effect",
  attack: "destinyjdr.sheet.attack",
  XPCostText: "destinyjdr.sheet.XPCostText",
  use: "destinyjdr.sheet.use",
  LVRequiredText: "destinyjdr.sheet.LVRequiredText",
  armorText: "destinyjdr.sheet.armor",
  personality: "destinyjdr.sheet.personality",
  physicaldescription: "destinyjdr.sheet.physicaldescription",
  healthmax: "destinyjdr.sheet.healthmax",
  health: "destinyjdr.sheet.health",
  resilience: "destinyjdr.sheet.resilience",
  meters: "destinyjdr.sheet.meters",
  doesdamage: "destinyjdr.sheet.doesdamage",
  doeseffects: "destinyjdr.sheet.doeseffects",
  overview: "destinyjdr.sheet.overview",
  createstruct: "destinyjdr.sheet.createstruct"
}

destinyjdr.skilltypes = {
  noroll: "destinyjdr.skilltypetext.noroll",
  simple1: "destinyjdr.skilltypetext.simple1",
  simple2: "destinyjdr.skilltypetext.simple2",
  damage1: "destinyjdr.skilltypetext.damage1",
  simple2damage: "destinyjdr.skilltypetext.simple2damage",
  damage2: "destinyjdr.skilltypetext.damage2"
}

destinyjdr.armorTypes = {
  head: "destinyjdr.armorText.head",
  arms: "destinyjdr.armorText.arms",
  chest: "destinyjdr.armorText.chest",
  legs: "destinyjdr.armorText.legs",
  class: "destinyjdr.armorText.class"
}

destinyjdr.actionTimes = {
  instant: "destinyjdr.actionTime.instant",
  semiaction: "destinyjdr.actionTime.semiaction",
  action: "destinyjdr.actionTime.action",
  actionPlus: "destinyjdr.actionTime.actionPlus",
  twoActions: "destinyjdr.actionTime.twoActions",
  twoActionsPlus: "destinyjdr.actionTime.twoActionPlus",
  threeActions: "destinyjdr.actionTime.threeActions",
  threeActionsPlus: "destinyjdr.actionTime.threeActionsPlus",
  fourActions: "destinyjdr.actionTime.fourActions",
  oneTurn: "destinyjdr.actionTime.oneTurn",
  twoTurns: "destinyjdr.actionTime.twoTurns",
  threeTurns: "destinyjdr.actionTime.threeTurns",
  fourTurns: "destinyjdr.actionTime.fourTurns",
  fiveTurns: "destinyjdr.actionTime.fiveTurns",
  sixTurns: "destinyjdr.actionTime.sixTurns",
  sevenTurns: "destinyjdr.actionTime.sevenTurns"
}

destinyjdr.buffs = {
  regeneration:"destinyjdr.statusEffectsBuffs.regeneration",
  unstoppable:"destinyjdr.statusEffectsBuffs.unstoppable",
  invulnerable:"destinyjdr.statusEffectsBuffs.invulnerable",
  travelersblessing:"destinyjdr.statusEffectsBuffs.travelersblessing",
  instantcelerity:"destinyjdr.statusEffectsBuffs.instantcelerity",
  strokeofgenius:"destinyjdr.statusEffectsBuffs.strokeofgenius",
  nightvision:"destinyjdr.statusEffectsBuffs.nightvision",
  invisible:"destinyjdr.statusEffectsBuffs.invisible",
  radiant:"destinyjdr.statusEffectsBuffs.radiant",
  amplified:"destinyjdr.statusEffectsBuffs.amplified",
  lightinfused:"destinyjdr.statusEffectsBuffs.lightinfused",
  lightblessed:"destinyjdr.statusEffectsBuffs.lightblessed",
  wovenmail:"destinyjdr.statusEffectsBuffs.wovenmail",
  rebuilt:"destinyjdr.statusEffectsBuffs.rebuilt",
  ravaged:"destinyjdr.statusEffectsBuffs.ravaged",
  augmented:"destinyjdr.statusEffectsBuffs.augmented"
}

destinyjdr.debuffs = {
  bleeding:"destinyjdr.statusEffectsDebuffs.bleeding",
  stun:"destinyjdr.statusEffectsDebuffs.stun",
  burning:"destinyjdr.statusEffectsDebuffs.burning",
  blind:"destinyjdr.statusEffectsDebuffs.blind",
  abyssmarked:"destinyjdr.statusEffectsDebuffs.abyssmarked",
  electrified:"destinyjdr.statusEffectsDebuffs.electrified",
  fear:"destinyjdr.statusEffectsDebuffs.fear",
  knockedout:"destinyjdr.statusEffectsDebuffs.knockedout",
  taunt:"destinyjdr.statusEffectsDebuffs.taunt",
  paralysed:"destinyjdr.statusEffectsDebuffs.paralysed",
  frozen:"destinyjdr.statusEffectsDebuffs.frozen",
  slowed:"destinyjdr.statusEffectsDebuffs.slowed",
  shiver:"destinyjdr.statusEffectsDebuffs.shiver",
  suspended:"destinyjdr.statusEffectsDebuffs.suspended",
  attached:"destinyjdr.statusEffectsDebuffs.attached",
  segmented:"destinyjdr.statusEffectsDebuffs.segmented",
  observed:"destinyjdr.statusEffectsDebuffs.observed"
}

destinyjdr.minstats = {
  CSstat:"destinyjdr.minstats.CSstat",
  ASstat:"destinyjdr.minstats.ASstat",
  STRstat:"destinyjdr.minstats.STRstat",
  DEXstat:"destinyjdr.minstats.DEXstat",
  RESstat:"destinyjdr.minstats.RESstat",
  INTstat:"destinyjdr.minstats.INTstat",
  PERstat:"destinyjdr.minstats.PERstat",
  MSstat:"destinyjdr.minstats.MSstat",
  CHstat:"destinyjdr.minstats.CHstat",
  TECstat:"destinyjdr.minstats.TECstat"
}

destinyjdr.simpleStats = [
  "combatskill",
  "aimingskill",
  "strength",
  "dexterity",
  "resilience",
  "intellect",
  "perception",
  "mentalstrength",
  "charisma",
  "technology"
]