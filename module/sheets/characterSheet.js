import * as Dice from "../dice.js";
import * as Tchat from "../tchat.js";
import {UpgradingStat} from "./upgrading-stat-sheet.js";
import {DestructionSave} from "./destruction-save-sheet.js";
import {CharInfosSheet} from "./character-informations-sheet.js";
import {ActiveEffectsDebugg} from "./debugg-sheet.js";

export default class characterSheet extends ActorSheet {

  /** @override */
  constructor(...args) {
    super(...args);
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      template: "systems/destinyjdr/templates/sheets/character-sheet.html",
      classes: ["destinyjdr", "sheet", "character"],
      width: 698,
      height: 745,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "stats" }]
    });
  }

  miscContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealCombatskill0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.combatskill")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.combatskill")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealAimingskill0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.aimingskill")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.aimingskill")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealStrength0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.strength")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.strength")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealDexterity0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.dexterity")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.dexterity")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealResilience0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.resilience")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.resilience")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealIntellect0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.intellect")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.intellect")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealPerception0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.perception")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.perception")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealMentalstrength0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.mentalstrength")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.mentalstrength")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealCharisma0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.charisma")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.charisma")))[0];
        return unreal;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.debugg.unrealTechnoogy0"),
      icon: '<i class="fas fa-times"></i>',
      callback: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.technology")))[0];
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [unreal.id]);
      },
      condition: element => {
        let unreal = this.actor.effects.filter(effect => effect.data.label.includes(game.i18n.localize("destinyjdr.unreal.technology")))[0];
        return unreal;
      }
    }
  ]

  statCombatskillContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollCombatskill"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "1";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theCombatskill");
        const theStat = 1;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statAimingskillContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollAimingskill"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "2";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theAimingskill");
        const theStat = 2;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statStrengthContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollStrength"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "3";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theStrength");
        const theStat = 3;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statDexterityContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollDexterity"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "4";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theDexterity");
        const theStat = 4;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statResilienceContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollResilience"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "5";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theResilience");
        const theStat = 5;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statIntellectContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollIntellect"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "6";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theIntellect");
        const theStat = 6;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statPerceptionContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollPerception"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "7";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.thePerception");
        const theStat = 7;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statMentalstrengthContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollMentalstrength"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "8";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theMentalstrength");
        const theStat = 8;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statCharismaContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollCharisma"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "9";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theCharisma");
        const theStat = 9;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  statTechnologyContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.rollTechnology"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const actualStat = element.data("action-value");
        const actualFatigue = element.data("fatigue-value");
        const statType = "10";
        let actorData = this.actor;

        Dice.StatCheck({
          actionValue: actualStat,
          statType: statType,
          actorData: actorData
        })
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.upgradeStat"),
      icon: '<i class="item-roll fas fa-arrow-circle-up"></i>',
      callback: element => {
        const actualUpgrade = element.data("upgrade-value");
        let actorData = this.actor;
        let upgradeText = game.i18n.localize("destinyjdr.sheet.upgradeText");
        let upgradeTextStat = game.i18n.localize("destinyjdr.sheet.theTechnology");
        const theStat = 10;
        const upgradingStat = new UpgradingStat(actorData, actualUpgrade, upgradeText, upgradeTextStat, theStat);
        upgradingStat.render(true);
      }
    }
  ]

  /*
  getTraitRollsContextMenu() {
    return Object.keys(CONFIG.destinyjdr.traitRolls).map( traitRollKey => (
      {
        name: game.i18n.localize(`destinyjdr.sheet.${traitRollKey}`),
        icon: '<i class="item-roll fas fa-dice-d20"></i>',
        callback: (element) => {
          Dice.TraitCheck(this.actor, traitRollKey);
        }
      }
    ));
  }
  */

  charStoryContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.edit"),
      icon: '<i class="fas fa-pen"></i>',
      callback: element => {
        const charInfo = "story";
        const charInfoSheet = new CharInfosSheet(this.actor, charInfo);
        charInfoSheet.render(true);
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.show"),
      icon: '<i class="item-roll fas fa-eye"></i>',
      callback: element => {
        Tchat.charStoryShow({
          actor: this.actor
        });
      }
    }
  ];

  charDescContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.edit"),
      icon: '<i class="fas fa-pen"></i>',
      callback: element => {
        const charInfo = "desc";
        const charInfoSheet = new CharInfosSheet(this.actor, charInfo);
        charInfoSheet.render(true);
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.show"),
      icon: '<i class="item-roll fas fa-eye"></i>',
      callback: element => {
        Tchat.charDescShow({
          actor: this.actor
        });
      }
    }
  ];

  itemContextMenu = [
    {
      name: game.i18n.localize("destinyjdr.sheet.edit"),
      icon: '<i class="fas fa-pen"></i>',
      callback: element => {
        const item = this.actor.items.get(element.data("item-id"));
        item.sheet.render(true);
      },
      condition: element => {
        if(element.data("armoractive") == "headactive" || element.data("armoractive") == "chestactive" || element.data("armoractive") == "armsactive" || element.data("armoractive") == "legsactive" || element.data("armoractive") == "classactive") {
          return false;
        } else {
          return true;
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use"),
      icon: '<i class="item-roll fas fa-bullseye"></i>',
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: "noroll",
          rollstat: "",
          rollmod: ""
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          if(skillType == "noroll") {
            return true;
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.CSstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "CSstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.ASstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "ASstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.STRstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "STRstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.DEXstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "DEXstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.RESstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "RESstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.INTstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "INTstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.PERstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "PERstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.MSstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "MSstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.CHstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "CHstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use1"),
      icon: game.i18n.localize("destinyjdr.iconstats.TECstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat1;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod1;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat1 = game.actors.get(actorID).items.get(itemID).system.rollstat1;
          if(skillType == "simple1" || skillType == "damage1" || skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat1 == "TECstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.CSstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "CSstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.ASstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "ASstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.STRstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "STRstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.DEXstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "DEXstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.RESstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "RESstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.INTstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "INTstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.PERstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "PERstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.MSstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "MSstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.CHstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "CHstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.use2"),
      icon: game.i18n.localize("destinyjdr.iconstats.TECstat"),
      callback: element => {
        const itemID = element.data("itemId");
        const actorID = this.actor.id;
        const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
        const rollstat = game.actors.get(actorID).items.get(itemID).system.rollstat2;
        const rollmod = game.actors.get(actorID).items.get(itemID).system.rollmod2;
        Tchat.useSkill({
          actorID: actorID,
          itemID: itemID,
          skillType: skillType,
          rollstat: rollstat,
          rollmod: rollmod
        });
      },
      condition: element => {
        if(element.data("itemType") === "skill") {
          const itemID = element.data("itemId");
          const actorID = this.actor.id;
          const skillType = game.actors.get(actorID).items.get(itemID).system.skilltype;
          const rollstat2 = game.actors.get(actorID).items.get(itemID).system.rollstat2;
          if(skillType == "simple2" || skillType == "damage2" || skillType == "simple2damage") {
            if(rollstat2 == "TECstat") {
              return true;
            }
          }
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.equipunequip"),
      icon: '<i class="item-roll fas fa-hands"></i>',
      callback: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);
        let armortype = item.system.armorType;
        if(armortype.includes("active")) {
          let objectsHeld = this.actor.items.filter(item => [ "weapon", "object", "armor" ].includes(item.type) );
          let armorheadheld = this.actor.items.filter(item => [ "headactive" ].includes(item.system.armorType) );
          let armorchestheld = this.actor.items.filter(item => [ "chestactive" ].includes(item.system.armorType) );
          let armorarmsheld = this.actor.items.filter(item => [ "armsactive" ].includes(item.system.armorType) );
          let armorlegsheld = this.actor.items.filter(item => [ "legsactive" ].includes(item.system.armorType) );
          let armorclassheld = this.actor.items.filter(item => [ "classactive" ].includes(item.system.armorType) );
          let numberofheldarmors = 0;
          armorheadheld.forEach( armor => {
            numberofheldarmors++;
          });
          armorchestheld.forEach( armor => {
            numberofheldarmors++;
          });
          armorarmsheld.forEach( armor => {
            numberofheldarmors++;
          });
          armorlegsheld.forEach( armor => {
            numberofheldarmors++;
          });
          armorclassheld.forEach( armor => {
            numberofheldarmors++;
          });
          if((objectsHeld.length)-numberofheldarmors >= 14) {
            ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchItems"));
            return false;
          }
          armortype = armortype.replace('active','');
          item.update({"system.armorType":armortype});
        } else {
          if(armortype == "head") {
            let armorheadheld = this.actor.items.filter(item => [ "headactive" ].includes(item.system.armorType) );
            let numberofheldarmors = 0;
            armorheadheld.forEach( armor => {
              numberofheldarmors++;
            });
            if(numberofheldarmors > 0) {
              ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsHead"));
              return false;
            }
          }
          if(armortype == "chest") {
            let armorchestheld = this.actor.items.filter(item => [ "chestactive" ].includes(item.system.armorType) );
            let numberofheldarmors = 0;
            armorchestheld.forEach( armor => {
              numberofheldarmors++;
            });
            if(numberofheldarmors > 0) {
              ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsChest"));
              return false;
            }
          }
          if(armortype == "arms") {
            let armorarmsheld = this.actor.items.filter(item => [ "armsactive" ].includes(item.system.armorType) );
            let numberofheldarmors = 0;
            armorarmsheld.forEach( armor => {
              numberofheldarmors++;
            });
            if(numberofheldarmors > 0) {
              ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsArms"));
              return false;
            }
          }
          if(armortype == "legs") {
            let armorlegsheld = this.actor.items.filter(item => [ "legsactive" ].includes(item.system.armorType) );
            let numberofheldarmors = 0;
            armorlegsheld.forEach( armor => {
              numberofheldarmors++;
            });
            if(numberofheldarmors > 0) {
              ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsLegs"));
              return false;
            }
          }
          if(armortype == "class") {
            let armorclassheld = this.actor.items.filter(item => [ "classactive" ].includes(item.system.armorType) );
            let numberofheldarmors = 0;
            armorclassheld.forEach( armor => {
              numberofheldarmors++;
            });
            if(numberofheldarmors > 0) {
              ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsClass"));
              return false;
            }
          }
          item.update({"system.armorType":armortype+"active"});
        }
      },
      condition: element => {
        if(element.data("itemType") == "armor") {
          return true;
        }
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.invoke"),
      icon: '<i class="item-roll fas fa-fire"></i>',
      callback: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);
        let actorID = this.actor.id;
        Tchat.superInvoke({
          itemID: itemID,
          actorID: actorID
        });
      },
      condition: element => {
        return element.data("itemType") === "super";
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.attack"),
      icon: '<i class="item-roll fas fa-dice-d20"></i>',
      callback: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);
        let actorData = this.actor;
        let rollData = {
          itemName: item.name,
          itemImg: item.img,
          rarity: item.system.rarity,
          damage: item.system.damage,
          element: item.system.element,
          weaponType: item.system.weaponType,
          weaponElement: item.system.weaponElement,
          weaponPenetration: item.system.weaponPenetration,
          perkactivated: item.system.perkactivated,
          perkname: item.system.perkname,
          perkid: item.system.perkid,
          perk: item.system.perk
        };
        Dice.weaponAttack({
          rollData: rollData,
          actorData: actorData
        });
      },
      condition: element => {
        return element.data("itemType") === "weapon";
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.destructionSave"),
      icon: '<i class="item-roll fas fa-bolt"></i>',
      callback: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);

        let actorID = this.actor.id;
        let destrSaveText = game.i18n.localize("destinyjdr.sheet.destructionSave2");

        const destrSave = new DestructionSave(itemID, actorID, destrSaveText);
    
        destrSave.render(true);
      },
      condition: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);
        let isWeaponOrObject = false;
        if(item) {
          if(item.type === "weapon" || item.type === "object" || item.type === "armor") {
            isWeaponOrObject = true;
          }
        }
        return isWeaponOrObject;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.show"),
      icon: '<i class="item-roll fas fa-eye"></i>',
      callback: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);
        let actorID = this.actor.id;
        Tchat.itemShow({
          itemData: item,
          actorID: actorID
        });
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.show+"),
      icon: '<i class="item-roll far fa-eye"></i>',
      callback: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);
        let actorID = this.actor.id;
        Tchat.itemShowMore({
          itemData: item,
          actorID: actorID
        });
      },
      condition: element => {
        const itemID = element.data("itemId");
        const item = this.actor.items.get(itemID);
        let isObject = false;
        if(item) {
          if(item.type === "weapon" || item.type === "object" || item.type === "super") {
            isObject = true;
          } else {
            isObject = false;
          }
        }
        return isObject;
      }
    },
    {
      name: game.i18n.localize("destinyjdr.sheet.delete"),
      icon: '<i class="fas fa-trash"></i>',
      callback: element => {
        this.actor.deleteEmbeddedDocuments("Item",[element.data("item-id")]);
      }
    }
  ];


  getData() {
    const data = super.getData();
    data.config = CONFIG.destinyjdr;

    data.races = this.actor.type === "personnage" ? CONFIG.destinyjdr.races : {...CONFIG.destinyjdr.races};

    data.ghost = data.items.filter(item => [ "ghost" ].includes(item.type) );
    data.equipables = data.items.filter(item => [ "weapon", "object", "armor" ].includes(item.type) );
    data.skills = data.items.filter(item => [ "skill" ].includes(item.type) );
    data.flaws = data.items.filter(item => [ "flaw" ].includes(item.type) );
    data.mutations = data.items.filter(item => [ "mutation" ].includes(item.type) );
    data.supers = data.items.filter(item => [ "super" ].includes(item.type) );
    data.traits = data.items.filter(item => [ "trait" ].includes(item.type) );
    
    data.armorhead = data.items.filter(item => [ "headactive" ].includes(item.system.armorType) );
    data.armorchest = data.items.filter(item => [ "chestactive" ].includes(item.system.armorType) );
    data.armorarms = data.items.filter(item => [ "armsactive" ].includes(item.system.armorType) );
    data.armorlegs = data.items.filter(item => [ "legsactive" ].includes(item.system.armorType) );
    data.armorclass = data.items.filter(item => [ "classactive" ].includes(item.system.armorType) );

    data.unrealStats = this.getUnrealStats();
    data.paracausal = this.getParacausal();

    data.isGM = game.user.isGM;

    return data;
  }

  activateListeners(html) {
    if(this.isEditable) {
      html.find(".character-infomain-bodyloc").click(this._onBodyLoc.bind(this));
      html.find(".character-infomain-meleeattack").click(this._onMeleeAttack.bind(this));
      html.find(".character-infomain-warlockmelee").click(this._onMeleeAttack.bind(this));

      html.find(".character-infomain-class").change(this._onClassChange.bind(this));

      html.find(".charactersheet-sidebar-stasisbutton").click(this._onCharacterFrozen.bind(this));
      
      html.find(".destinyjdr-rollinitiative").click(this._onRollInitiative.bind(this));

      html.find('.open-debugg').click(this._onButtonDebugg.bind(this));

      new ContextMenu(html, ".item-charinfostory", this.charStoryContextMenu);
      new ContextMenu(html, ".item-charinfodesc", this.charDescContextMenu);

      new ContextMenu(html, ".item-card", this.itemContextMenu);

      new ContextMenu(html, ".stat-bloc-combatskill", this.statCombatskillContextMenu);
      new ContextMenu(html, ".stat-bloc-aimingskill", this.statAimingskillContextMenu);
      new ContextMenu(html, ".stat-bloc-strength", this.statStrengthContextMenu);
      new ContextMenu(html, ".stat-bloc-dexterity", this.statDexterityContextMenu);
      new ContextMenu(html, ".stat-bloc-resilience", this.statResilienceContextMenu);
      new ContextMenu(html, ".stat-bloc-intellect", this.statIntellectContextMenu);
      new ContextMenu(html, ".stat-bloc-perception", this.statPerceptionContextMenu);
      new ContextMenu(html, ".stat-bloc-mentalstrength", this.statMentalstrengthContextMenu);
      new ContextMenu(html, ".stat-bloc-charisma", this.statCharismaContextMenu);
      new ContextMenu(html, ".stat-bloc-technology", this.statTechnologyContextMenu);

      //Owner-only listeners
      if (this.actor.isOwner) {
        /*html.find(".task-check").click(this._onTaskCheck.bind(this));*/
      }
    }
    super.activateListeners(html);
  }

  _onRollInitiative(event) {
    event.preventDefault();
    const actorData = event.currentTarget.dataset.actor;
    const dexterityValue = event.currentTarget.dataset.dex;
    const perceptionValue = event.currentTarget.dataset.per;

    Dice.initiativeCheck({
      dexterityValue: dexterityValue,
      perceptionValue: perceptionValue,
      actorData: actorData
    });
  }

  _onBodyLoc(event) {
    event.preventDefault();
    const raceNum = this.actor.system.race;
    
    const actorData = this.actor;

    Dice.LocCheck({
      raceNum: raceNum,
      actorData: actorData
    });
  }

  _onButtonDebugg(event) {
    event.preventDefault();
    const leDebugg = new ActiveEffectsDebugg(this.actor);

    leDebugg.render(true);
  }

  _onMeleeAttack(event) {
    event.preventDefault();
    const type = event.currentTarget.dataset.type;
    Tchat.MeleeAttack({
      actorID: this.actor._id,
      type: type
    });
  }

  _onItemRoll(event) {
    const itemID = event.currentTarget.closest(".weapon-card").set.itemId;
    const item = this.actor.items.get(itemID);

    item.roll();
  }

  _onItemEdit(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemId = element.closest(".weapon-card").set.itemId;
    let item = this.actor.items.get(itemId);

    item.sheet.render(true);
  }

  _onItemDelete(event) {
    event.preventDefault();
    let element = event.currentTarget;
    let itemId = element.closest(".weapon-card").set.itemId;
    return this.actor.deleteEmbeddedDocuments("Item",[itemId]);
  }

  async _onDropItem(event, data) {
    event.preventDefault();

    let theItemInfos = game.items.get(data.uuid.substring(5));
    let theItem = theItemInfos.type;

    let ghostsHeld = this.actor.items.filter(item => [ "ghost" ].includes(item.type) );
    let objectsHeld = this.actor.items.filter(item => [ "weapon", "object", "armor" ].includes(item.type) );
    let skillsHeld = this.actor.items.filter(item => [ "skill" ].includes(item.type) );
    let flawsmutationsHeld = this.actor.items.filter(item => [ "flaw", "mutation" ].includes(item.type) );
    let supersHeld = this.actor.items.filter(item => [ "super" ].includes(item.type) );
    let traitsHeld = this.actor.items.filter(item => [ "trait" ].includes(item.type) );

    let armorheadheld = this.actor.items.filter(item => [ "headactive" ].includes(item.system.armorType) );
    let armorchestheld = this.actor.items.filter(item => [ "chestactive" ].includes(item.system.armorType) );
    let armorarmsheld = this.actor.items.filter(item => [ "armsactive" ].includes(item.system.armorType) );
    let armorlegsheld = this.actor.items.filter(item => [ "legsactive" ].includes(item.system.armorType) );
    let armorclassheld = this.actor.items.filter(item => [ "classactive" ].includes(item.system.armorType) );

    let numberofheldarmors = 0;
    armorheadheld.forEach( armor => {
      numberofheldarmors++;
    });
    armorchestheld.forEach( armor => {
      numberofheldarmors++;
    });
    armorarmsheld.forEach( armor => {
      numberofheldarmors++;
    });
    armorlegsheld.forEach( armor => {
      numberofheldarmors++;
    });
    armorclassheld.forEach( armor => {
      numberofheldarmors++;
    });

    if(theItem == "weapon") {
      if((objectsHeld.length)-numberofheldarmors >= 14) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchItems"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

    if(theItem == "object") {
      if((objectsHeld.length)-numberofheldarmors >= 14) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchItems"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

    if(theItem == "ghost") {
      if(ghostsHeld.length >= 1) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchGhosts"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

    if(theItem == "trait") {
      if(traitsHeld.length >= 22) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchTraits"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

    if(theItem == "skill") {
      if(skillsHeld.length >= 6) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchSkills"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

    if(theItem == "super") {
      if(supersHeld.length >= 2) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchSupers"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

    if(theItem == "armor") {
      if((objectsHeld.length)-numberofheldarmors >= 14) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchItems"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

    /*
    if(theItem == "armor") {

      if(armortype == "head") {
        let alreadyhavearmor = 0;
        armorsHeld.forEach((item, index) => {
          if(item.system.armorType == "head") {
            alreadyhavearmor = 1;
          }
        });
        if(alreadyhavearmor == 0) {
          return super._onDropItem(event, data);
        } else {
          ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsHead"));
          return false;
        }
      }
      if(armortype == "arms") {
        let alreadyhavearmor = 0;
        armorsHeld.forEach((item, index) => {
          if(item.system.armorType == "arms") {
            alreadyhavearmor = 1;
          }
        });
        if(alreadyhavearmor == 0) {
          return super._onDropItem(event, data);
        } else {
          ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsArms"));
          return false;
        }
      }
      if(armortype == "chest") {
        let alreadyhavearmor = 0;
        armorsHeld.forEach((item, index) => {
          if(item.system.armorType == "chest") {
            alreadyhavearmor = 1;
          }
        });
        if(alreadyhavearmor == 0) {
          return super._onDropItem(event, data);
        } else {
          ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsChest"));
          return false;
        }
      }
      if(armortype == "legs") {
        let alreadyhavearmor = 0;
        armorsHeld.forEach((item, index) => {
          if(item.system.armorType == "legs") {
            alreadyhavearmor = 1;
          }
        });
        if(alreadyhavearmor == 0) {
          return super._onDropItem(event, data);
        } else {
          ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsLegs"));
          return false;
        }
      }
      if(armortype == "class") {
        let alreadyhavearmor = 0;
        armorsHeld.forEach((item, index) => {
          if(item.system.armorType == "class") {
            alreadyhavearmor = 1;
          }
        });
        if(alreadyhavearmor == 0) {
          return super._onDropItem(event, data);
        } else {
          ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchArmorsClass"));
          return false;
        }
      }
    }
    */

    if(theItem == "flaw" || theItem == "mutation") {
      if(flawsmutationsHeld.length >= 6) {
        ui.notifications.warn(game.i18n.localize("destinyjdr.notifications.tooMuchFlawsOrMutations"));
        return false;
      } else {
        return super._onDropItem(event, data);
      }
    }

  }

  _onClassChange(event) {
    const actorID = event.currentTarget.closest(".character-infomain-class").dataset.actor;
    let actorclass;
    setTimeout(() => {
      actorclass = game.actors.get(actorID).system.class;
      if(actorclass == "hunter") {
        game.actors.get(actorID).update({"system.subclass": "gunslinger"});
      }
      if(actorclass == "titan") {
        game.actors.get(actorID).update({"system.subclass": "sunbreaker"});
      }
      if(actorclass == "warlock") {
        game.actors.get(actorID).update({"system.subclass": "dawnblade"});
      }
    }, 100);
  }

  _onCharacterFrozen(event) {
    event.preventDefault();
    event.stopPropagation();
    const actorID = event.currentTarget.dataset.actor;
    const itemID = event.currentTarget.dataset.item;
    /*
    let applyTheDebuff = game.actors.get(actorID).effects.filter(effect => effect.data.label === game.i18n.localize("destinyjdr.statusEffectsDebuffs.frozen"))[0];
    if(applyTheDebuff){return;}
    let theDebuff = {
      label:game.i18n.localize("destinyjdr.statusEffectsDebuffs.frozen"),
      icon: "systems/destinyjdr/assets/img/effects/frozen.svg",
      changes:[],
      duration: {
        rounds: 1
      }
    };
    applyTheDebuff = ActiveEffect.create(theDebuff, {parent: game.actors.get(actorID)});
    */
    Tchat.createStruct({
      actorID: actorID,
      itemID: itemID,
      type: "stasis"
    });
  }

  getUnrealStats() {
    return Object.keys(CONFIG.destinyjdr.stats).reduce( (acc,cur) => {
      const unreal = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize(`destinyjdr.unreal.${cur}`)))[0];
      return unreal ? {...acc,[cur]:"unrealclass"} : acc;
    },{});
  }

  getParacausal() {
    const para = this.actor.effects.filter(effect => effect.label.includes(game.i18n.localize(`destinyjdr.sheet.paracausal`)))[0];
    return para;
  }

}
