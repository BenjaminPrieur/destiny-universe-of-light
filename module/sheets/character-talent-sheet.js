export class TalentsSheet extends FormApplication {

  /** @override */
  constructor(actor, race, compname, compdesc, compname1, talentSkillType) {
    super(actor, {
      closeOnSubmit: false,
      submitOnChange: true,
      submitOnClose: true,
      title: game.i18n.localize("destinyjdr.sheettext."+talentSkillType)
     });

    this.actor = actor;
    this.race = race;
    this.compname = compname;
    this.compdesc = compdesc;
    this.compname1 = compname1;
    this.talentSkillType = talentSkillType;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["destinyjdr", "sheet", "character"],
      template: 'systems/destinyjdr/templates/sheets/talent-sheet.html',
      width: 600,
      height: 390
    });
  }

  /** @override */
  getData() {
    const sheetData = {};

    // The Actor's data
    const actorData = this.actor.toObject(false);
    sheetData.actor = actorData;
    sheetData = actorData;
    sheetData.race = this.race;
    sheetData.compname = this.compname;
    sheetData.compdesc = this.compdesc;
    sheetData.compname1 = this.compname1;
    sheetData.talentSkillType = this.talentSkillType;

    return sheetData;
  }

  /** @inheritdoc */
  async _updateObject(event, formData) {
    if ( !this.object.id ) return;
    return this.object.update(formData);
  }
}