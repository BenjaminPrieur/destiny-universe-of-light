export class CharInfosSheet extends FormApplication {

  /** @override */
  constructor(actor, charInfo) {
    super(actor, {
      closeOnSubmit: false,
      submitOnChange: true,
      submitOnClose: true,
      title: actor.name
     });

    this.actor = actor;
    this.charInfo = charInfo;
  }

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["destinyjdr", "sheet", "character"],
      template: 'systems/destinyjdr/templates/sheets/char-infos-sheet.html',
      width: 400,
      height: 300
    });
  }

  /** @override */
  getData() {
    let sheetData = {};

    // The Actor's data
    const actorData = this.actor.toObject(false);
    sheetData.actor = actorData;
    sheetData = actorData;
    sheetData.charInfo = this.charInfo;

    return sheetData;
  }

  /** @inheritdoc */
  async _updateObject(event, formData) {
    if ( !this.object.id ) return;
    return this.object.update(formData);
  }
}