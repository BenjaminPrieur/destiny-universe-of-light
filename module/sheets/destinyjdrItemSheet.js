import * as Dice from "../dice.js";
import {WeaponTalentSheet} from "./weapontalent-sheet.js";

export default class destinyjdrItemSheet extends ItemSheet {

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 630,
      height: 318,
      classes: ["destinyjdr", "sheet", "item"]
    })
  }

  //Override
  activateListeners(html) {
    super.activateListeners(html);
    html.find("#destinyjdr-item-button-shoot").click(this._onItemSheetShoot.bind(this));
    html.find('.destinyjdr-item-perkbutton.activated').click(this._onTalentEdit.bind(this));
  }

  get template() {
    return `systems/destinyjdr/templates/sheets/${this.item.type}-sheet.html`;
  }

  //Override
  getData() {
    const data = super.getData();

    data.config = CONFIG.destinyjdr;
    data.isGM = game.user.isGM;

    return data;
  }

  _onItemSheetShoot(event) {
    const selectElem = event.currentTarget;
    event.currentTarget.closest(".destinyjdr-item-containerdouble").getElementsByClassName("weaponmagazineleft")[0].value = event.currentTarget.closest(".destinyjdr-item-containerdouble").getElementsByClassName("weaponmagazineleft")[0].value - selectElem.classList[1].slice(-1);
  }

  _onTalentEdit(event) {
    event.preventDefault();
    let element = event.currentTarget;
    const itemID = element.dataset.itemId;
    let item = game.items.get(itemID);
    if (typeof item !== "undefined") {} else {
      const actor = game.actors.get(event.currentTarget.closest(".destinyjdr-item-perkbutton").dataset.actorId);
      item = actor.items.get(itemID);
    }
    const weaponTalentSheet = new WeaponTalentSheet(item);
    weaponTalentSheet.render(true);
  }
}
