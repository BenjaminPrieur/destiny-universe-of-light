import { destinyjdr } from "./module/config.js";
import * as Tchat from "./module/tchat.js";
import destinyjdrItemSheet from "./module/sheets/destinyjdrItemSheet.js";
import destinyjdrItem from "./module/destinyjdrItem.js";
import destinyjdrActor from "./module/destinyjdrActor.js";
import destinyjdrCombat from "./module/combat.js";
import destinyjdrCombatTracker from "./module/combatTracker.js";
import characterSheet from "./module/sheets/characterSheet.js";
import { registerSystemSettings } from "./module/settings.js";

async function preloadHandlebarsTemplates() {
  const templatePaths = [
    "systems/destinyjdr/templates/partials/item-card.html",
    "systems/destinyjdr/templates/partials/weapon-card.html",
    "systems/destinyjdr/templates/partials/tchat-card.html",
    "systems/destinyjdr/templates/partials/weapon-chat.html",
    "systems/destinyjdr/templates/partials/charactersheet-sidebar-block.html",
    "systems/destinyjdr/templates/partials/charactersheet-main-header-block.html",
    "systems/destinyjdr/templates/partials/charactersheet-main-buttons-block.html",
    "systems/destinyjdr/templates/partials/charactersheet-main-content-bar1-block.html",
    "systems/destinyjdr/templates/partials/charactersheet-main-content-bar2-block.html",
    "systems/destinyjdr/templates/partials/charactersheet-main-allegiance-points-block.html",
    "systems/destinyjdr/templates/partials/charactersheet-main-allegiance-tree-block.html",
    "templates/dice/roll.html"
  ];

  return loadTemplates(templatePaths);
}

Hooks.once("init", async function() {
  console.log("destinyjdr | Initialising Destiny: Universe of Light System");

  CONFIG.destinyjdr = destinyjdr;
  CONFIG.Combat.entityClass = destinyjdrCombat;
  CONFIG.ui.combat = destinyjdrCombatTracker;

  //game.socket.on('system.destinyjdr', Tchat.onSocketReceived);

  CONFIG.Item.documentClass = destinyjdrItem;
  CONFIG.Actor.documentClass = destinyjdrActor;

  CONFIG.ChatMessage.documentClass = destinyChatMessage;
  CONFIG.ChatMessage.template = "systems/destinyjdr/templates/chat/chat-message.html";

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("destinyjdr", destinyjdrItemSheet, { makeDefault: true });

  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("destinyjdr", characterSheet, { makeDefault: true });

  registerSystemSettings();

  await preloadHandlebarsTemplates();

  registerHandlebarsHelpers();

  registerInitiative();
});

Hooks.on("renderChatLog", (app, html, data) => {
  html.on("click",".roll.die:not(.diererolled)",_onRerollTchatDice);
  html.on("click",".diererolled",_onRerollTchatDice);
  html.on("click",".destinyjdr-damageorheal",_onSkillDamage);
  html.on("click",".destinyjdr-superbutton",_onSuperEvent);
  html.on("click",".destinyjdr-skillbutton",_onSkillEvent);
  html.on("click",".destinyjdr-monolithbutton",_onMonolithEvent);
  html.on("click",".destinyjdr-audio",_onAudioEvent);
});

Hooks.on("renderChatMessage", (app, html, data) => {
  Tchat.chatListeners(html, data);
  if(!app._roll){return;}
  html.on("change",".input-struct",_onStructChange);
});

Hooks.once('ready', async function () {

});

function registerHandlebarsHelpers() {
  Handlebars.registerHelper('ifSuperiorOrEquals', function(arg1, arg2, options) {
    return (arg1 >= arg2) ? options.fn(this) : options.inverse(this);
  });

  Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });

  Handlebars.registerHelper('ifNotEquals', function(arg1, arg2, options) {
    return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
  });

  Handlebars.registerHelper('critString', function(dice) {
    let returnString = '';
    dice.forEach( mydie => {
      if(mydie.results) {
        mydie.results.forEach( result => {
          if( 4 !== mydie.faces && result.result === mydie.faces ) {
            returnString = game.i18n.localize('destinyjdr.critical.critdamage');
          }
        });
      }
    });
    return returnString;
  });

  Handlebars.registerHelper('bonusNum', function(num) {
    let newnum = num.toString();
    return newnum.substring(0,newnum.length-1);
  });

  Handlebars.registerHelper('addition', function(num1, num2) {
    if(num2 == "") {
      num2 = "0";
    }
    return String(parseInt(num1)+parseInt(num2));
  });

  Handlebars.registerHelper('critTotal', function(dice) {
    let returnString = '';
    dice.forEach( die => {
      die.results.forEach( result => {
        if( 4 !== die.faces && result.result === die.faces ) {
          returnString = 'tvq-criticaltotal';
        }
      });
    });
    return returnString;
  });

  Handlebars.registerHelper('critRoll', function(dice) {
    let returnString = 'display: none';
    dice.forEach( die => {
      die.results.forEach( result => {
        if(4 !== die.faces && result.result === die.faces) {
          returnString = 'display: block';
        }
      });
    });
    return returnString;
  });

  Handlebars.registerHelper('getStrengthOfActor', function(id) {
    let actors = game.actors;
    let strength = "FOR";
    actors.forEach(actor => {
      actor.items.forEach(item => {
        if(item._id == id) {
          strength = String(actor.system.stats.strength).substring(0,String(actor.system.stats.strength).length-1);
        }
      });
    });
    if(strength == "") {
      strength = "0";
    }
    return strength;
  });

  Handlebars.registerHelper('whichCrit', function(dice) {
    let critFord6 = Math.floor(Math.random() * 4)+1;
    let critFord8 = Math.floor(Math.random() * 6)+1;
    let critFord10 = Math.floor(Math.random() * 8)+1;
    let returnString = '';
    let maxima = 6;
    dice.forEach( die => {
      die.results.forEach( result => {
        if( 4 !== die.faces && result.result === die.faces ) {
          if(maxima < die.faces) {
            maxima = die.faces;
          }
          if( maxima === 8 ) {
            returnString = critFord8;
          } else {
            if( maxima === 10 ) {
              returnString = critFord10;
            } else {
              returnString = critFord6;
            }
          }
        }
      });
    });
    return returnString;
  });

  Handlebars.registerHelper('whichCritDice', function(dice) {
    let returnString = "";
    let maxima = 6;
    dice.forEach( die => {
      die.results.forEach( result => {
        if( 4 !== die.faces && result.result === die.faces ) {

          /* result.result = valeur du dé qui a crit */

          if(maxima < die.faces) {
            maxima = die.faces;
          }
  
          if( maxima === 8 ) {
            returnString = "d6";
          } else {
            if( maxima === 10 ) {
              returnString = "d8";
            } else {
              returnString = "d4";
            }
          }
        }
      });
    });
    return returnString;
  });

  Handlebars.registerHelper('forLoop', function (nbIterr, loopInner) {
    return [...Array(nbIterr)].reduce((acc, cur, index) => (acc + loopInner.fn(index)), "");
  });

  //github.com/adg29/concat.js
  Handlebars.registerHelper('concat', function () {
    let outStr = '';
    for (const arg in arguments) {
      if (typeof arguments[arg] !== 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('compareRanges', function (weaponrange, scoperange) {
    let returnString = game.i18n.localize('destinyjdr.weaponscopemessage.good');
    let difference = scoperange-weaponrange;
    if(difference >= 25) {
      returnString = game.i18n.localize('destinyjdr.weaponscopemessage.bad1');
    }
    if(difference >= 40) {
      returnString = game.i18n.localize('destinyjdr.weaponscopemessage.bad2');
    }
    if(difference >= 50) {
      returnString = game.i18n.localize('destinyjdr.weaponscopemessage.bad3');
    }
    if(difference >= 60) {
      returnString = game.i18n.localize('destinyjdr.weaponscopemessage.bad4');
    }
    return returnString;
  });

  Handlebars.registerHelper('compareRanges2', function (weaponrange, scoperange) {
    let returnString = "green";
    let difference = scoperange-weaponrange;
    if(difference >= 25) {
      returnString = "red";
    }
    return returnString;
  });

  Handlebars.registerHelper('getTableName', function (flavor) {
    /*
    let table = "";
    for (let t of game.packs.filter(p => p.documentName === "RollTable")) {
      table = await t.getDocument(docId);
    };
    const tablename = table.name;
    */
    return flavor.substring(24, flavor.length - 7);
  });

  Handlebars.registerHelper('ifExists', function (elem) {
    const theElem = elem;
    let answer = false;

    if(theElem !== "undefined") {
      answer = true;
    }

    return answer;
  });

  Handlebars.registerHelper("includes", includes);
  function includes(string, searchString) {
    return string?.includes(searchString) ?? false;
  }

  Handlebars.registerHelper("notContains", notContains);
  function notContains(string, searchString) {
    if(string.includes(searchString)) {
      return false;
    } else {
      return true;
    }
  }

  Handlebars.registerHelper("contains", contains);
  function contains(string, searchString) {
    if(string.includes(searchString)) {
      return true;
    } else {
      return false;
    }
  }

  Handlebars.registerHelper('ifStringContainsGroup', function(arg1, arg2, options) {
    let text1 = "g"+arg1.toString();
    let text2 = arg2.toString();
    return (text2.includes(text1)) ? options.fn(this) : options.inverse(this);
  });

  Handlebars.registerHelper('ifStringContainsDice', function(arg1, arg2, options) {
    let text1 = "d"+arg1.toString();
    let text2 = arg2.toString();
    return (text2.includes(text1)) ? options.fn(this) : options.inverse(this);
  });

  Handlebars.registerHelper('ifStringContainsGroupAndDice', function(arg1, arg2, arg3, options) {
    let text1 = "g"+arg1.toString();
    let text2 = "d"+arg2.toString();
    let is_contains = false;

    arg3.forEach(function(couple) {
      if(couple.includes(text1) && couple.includes(text2)) {
        is_contains = true;
      }
  });
    return (is_contains) ? options.fn(this) : options.inverse(this);
  });

}

function registerInitiative() {
  Combatant.prototype._getInitiativeFormula = function () {

    const actorData = this.actor;

    let agiNum = Math.floor(actorData.system.stats.dexterity / 10);
    let perNum = Math.floor(actorData.system.stats.perception / 10);

    const formula = `1d10 + ${agiNum} + ${perNum}`;
    return formula;
  }
}

class destinyChatMessage extends ChatMessage {
  async getHTML() {
    if(this.flags.core?.initiativeRoll) {
      if(this.isContentVisible) {
        return super.getHTML();
      }
    } else {
      return super.getHTML();
    }
  }
}

async function _onRerollTchatDice(event) {
  event.preventDefault();
  event.stopPropagation();
  const messageID = event.currentTarget.closest(".chat-message").dataset.messageId;
  const actorID = event.currentTarget.closest(".dice-roll").dataset.actorId;
  const group_position = event.currentTarget.closest(".rolls-group").dataset.group;
  let dice_position = event.currentTarget.closest(".roll.die").dataset.position;
  const faces = event.currentTarget.dataset.faces;

  const message = game.messages.get(messageID);

  const attacktype = event.currentTarget.closest(".dice-roll").dataset.attacktype;
  let crit1damage;
  let crit1number;
  let damage1level;
  let element;
  let error1;
  let fixednumber1;
  let img;
  let isreroll1;
  let name;
  let rollResult1;

  if(attacktype == "super") {
    crit1damage = message.flags.attackmessage.crit1damage;
    crit1number = message.flags.attackmessage.crit1number;
    damage1level = message.flags.attackmessage.damage1level;
    element = message.flags.attackmessage.element;
    error1 = message.flags.attackmessage.error;
    fixednumber1 = message.flags.attackmessage.fixednumber;
    img = message.flags.attackmessage.img;
    isreroll1 = message.flags.attackmessage.isreroll1;
    name = message.flags.attackmessage.name;
    rollResult1 = message.flags.attackmessage.rollResult;
  }

  const rollData = message.flags.attackmessage.rollData;
  const rollResult = message.flags.attackmessage.rollResult;
  const rarity = message.flags.attackmessage.rarity;
  const weaponType = message.flags.attackmessage.weaponType;
  const weaponElement = message.flags.attackmessage.weaponElement;
  const weaponPenetration = message.flags.attackmessage.weaponPenetration;
  const perkactivated = message.flags.attackmessage.perkactivated;
  const perkname = message.flags.attackmessage.perkname;
  const perkid = message.flags.attackmessage.perkid;
  const perk = message.flags.attackmessage.perk;
  const fixednumber = message.flags.attackmessage.fixednumber;
  const critnumber = message.flags.attackmessage.critnumber;
  const critdamage = message.flags.attackmessage.critdamage;
  const error = message.flags.attackmessage.error;
  const meleeattack = message.flags.attackmessage.meleeattack;
  const meleeelement = message.flags.attackmessage.meleeelement;
  let array_of_rerolls_infos = message.flags.attackmessage.array_of_rerolls_infos;

  if(dice_position == "" && event.currentTarget.closest(".rerolledminigroup").dataset.position) {
    dice_position = event.currentTarget.closest(".rerolledminigroup").dataset.position;
  }

  let die = "d"+faces;
  let newRollResult = new Roll(die, actorID);
  await newRollResult.evaluate({async:true});

  newRollResult.options.group_position = group_position;
  newRollResult.options.dice_position = dice_position;
  
  let array_of_rerolls_dicevalue = message.flags.attackmessage.array_of_rerolls_dicevalue;
  if(!array_of_rerolls_dicevalue) {
    array_of_rerolls_dicevalue = [newRollResult];
  } else {
    array_of_rerolls_dicevalue.push(newRollResult);
  }

  let array_of_rerolls_group = "g"+group_position;
  if(message.flags.attackmessage.array_of_rerolls_group) {
    array_of_rerolls_group = message.flags.attackmessage.array_of_rerolls_group+"-"+array_of_rerolls_group;
  }

  let array_of_rerolls_dice = "d"+dice_position;
  if(message.flags.attackmessage.array_of_rerolls_dice) {
    array_of_rerolls_dice = message.flags.attackmessage.array_of_rerolls_dice+"-"+array_of_rerolls_dice;
  }

  if(!array_of_rerolls_infos) {
    array_of_rerolls_infos = [];
  }

  if(array_of_rerolls_infos.length == 0) {
    array_of_rerolls_infos[0] = "g"+group_position+"-d"+dice_position;
  } else {
    array_of_rerolls_infos.push("g"+group_position+"-d"+dice_position);
  }

  let rerolls_time = 0;
  array_of_rerolls_infos.forEach(function() {
    rerolls_time++;
  });

  let array_of_rererolls = message.flags.attackmessage.array_of_rererolls;

  let damagelevel = message.flags.attackmessage.damagelevel

  Tchat.reRollingTchatDice({
    messageID: messageID,
    actorID: actorID,
    group_position: group_position,
    dice_position: dice_position,
    rollData: rollData,
    rollResult: rollResult,
    rarity: rarity,
    weaponType: weaponType,
    weaponElement: weaponElement,
    weaponPenetration: weaponPenetration,
    perkactivated: perkactivated,
    perkname: perkname,
    perkid: perkid,
    perk: perk,
    fixednumber: fixednumber,
    critnumber: critnumber,
    critdamage: critdamage,
    error: error,
    array_of_rerolls_group: array_of_rerolls_group,
    array_of_rerolls_dice: array_of_rerolls_dice,
    array_of_rerolls_dicevalue: array_of_rerolls_dicevalue,
    array_of_rererolls: array_of_rererolls,
    newRollResult: newRollResult,
    array_of_rerolls_infos: array_of_rerolls_infos,
    attacktype: attacktype,
    crit1damage: crit1damage,
    crit1number: crit1number,
    damage1level: damage1level,
    element: element,
    fixednumber1: fixednumber1,
    img: img,
    name: name,
    rollResult1: rollResult1,
    meleeattack: meleeattack,
    meleeelement: meleeelement,
    damagelevel: damagelevel
  });
}

async function _onSkillDamage(event) {
  event.preventDefault();
  event.stopPropagation();
  const actorID = event.currentTarget.dataset.actor;
  const itemID = event.currentTarget.dataset.item;
  const dicedamage = event.currentTarget.dataset.dicedamage;
  const fixeddamage = event.currentTarget.dataset.fixeddamage;
  const damagestat = event.currentTarget.dataset.damagestat;
  const damageelement = event.currentTarget.dataset.damageelement;
  Tchat.superSkillDamage({
    actorID: actorID,
    img: game.actors.get(actorID).items.get(itemID).img,
    name: game.actors.get(actorID).items.get(itemID).name,
    element: damageelement,
    dicedamage: dicedamage,
    fixeddamage: fixeddamage,
    damagestat: damagestat,
    type: 3
  });
}

async function _onSuperEvent(event) {
  event.preventDefault();
  event.stopPropagation();
  const actorID = event.currentTarget.dataset.actor;
  const choice = event.currentTarget.dataset.choice;
  const img = event.currentTarget.dataset.img;
  const name = event.currentTarget.dataset.name;
  const element = event.currentTarget.dataset.element;
  if(choice == "1") {
    const damagedicenum = event.currentTarget.dataset.damagedicenum;
    const damagedicetype = event.currentTarget.dataset.damagedicetype;
    const damagelevel = event.currentTarget.dataset.damagelevel;
    const dicedamage = damagedicenum+"d"+damagedicetype;
    const fixednumber = event.currentTarget.dataset.fixednumber;
    Tchat.superSkillDamage({
      actorID: actorID,
      img: img,
      name: name,
      element: element,
      dicedamage: dicedamage,
      fixeddamage: fixednumber,
      damagestat: damagelevel,
      type: 0
    });
  } else {
    const effect = event.currentTarget.dataset.effect;
    Tchat.superEffect({
      actorID: actorID,
      img: img,
      name: name,
      element: element,
      effect: effect
    });
  }
}

async function _onSkillEvent(event) {
  event.preventDefault();
  event.stopPropagation();
  const actorID = event.currentTarget.dataset.actor;
  const itemID = event.currentTarget.dataset.item;
  Tchat.createStruct({
    actorID: actorID,
    itemID: itemID,
    type: "skill"
  });
}

async function _onMonolithEvent(event) {
  event.preventDefault();
  event.stopPropagation();
  const actorID = event.currentTarget.dataset.actor;
  const itemID = event.currentTarget.dataset.item;
  Tchat.createStruct({
    actorID: actorID,
    itemID: itemID,
    type: "monolith"
  });
}

async function _onAudioEvent(event) {
  event.preventDefault();
  event.stopPropagation();
  const audioURL = event.currentTarget.dataset.audio;
  const audioFile = new Audio(audioURL);
  audioFile.volume=.35;
  audioFile.play();
}